package main.java.enums;

public enum EnvironmentType {
	LOCAL,
	REMOTE,
	STAGING,
	PRODUCTION	
}
