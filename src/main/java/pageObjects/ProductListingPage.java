package main.java.pageObjects;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class ProductListingPage {

	WebDriver driver;
	public ProductListingPage(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
		this.driver = driver;
	}
	
	@FindBy(how = How.CSS, using = "button.single_add_to_cart_button") 
	 private WebElement btn_AddToCart;
	 
	 @FindAll(@FindBy(how = How.CSS, using = ".noo-product-inner"))
	 private List<WebElement> prd_List; 
	 
	 public void clickOn_AddToCart() {
	 btn_AddToCart.click();
	 }
	 
	 public void select_Product(int productNumber) {
	 prd_List.get(productNumber).click();
	 Select colorDropDown = new Select(driver.findElement(By.xpath("//*[@id='pa_color']")));
	 colorDropDown.selectByVisibleText("White");
	 Select sizeDropDown = new Select(driver.findElement(By.xpath("//*[@id='pa_size']")));
	 sizeDropDown.selectByVisibleText("Medium");
	 }
}
