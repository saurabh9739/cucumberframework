package main.java.pageObjects;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import main.java.context.TestContext;
import main.java.managers.FileReaderManager;


public class HomePage {

	WebDriver driver;
	public HomePage(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
		this.driver = driver;
	}
	
	public void navigateTo_HomePage() {
		driver.get(FileReaderManager.getFileReaderManagerInstance().getConfigFileReaderInstance().getApplicationUrl());
	}
	
	public void perform_Search(String search) {
		driver.navigate().to("http://shop.demoqa.com/?s=" + search + "&post_type=product");
	}
}
