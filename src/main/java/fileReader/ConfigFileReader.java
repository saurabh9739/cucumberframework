package main.java.fileReader;

import java.io.File;
import java.io.FileReader;
import java.util.Properties;

import main.java.enums.DriverType;
import main.java.enums.EnvironmentType;

public class ConfigFileReader {
	
	Properties properties;
	File file;
	FileReader reader;
	public ConfigFileReader()
	{
		properties = new Properties();
	    file= new File("C:\\Users\\Saura\\eclipse-workspace\\CucumberFramework\\src\\main\\java\\configs\\configuration.properties");
	    try {
	    	reader = new FileReader(file);
	    	try {
	    		properties.load(reader);
	    	}catch(Exception e)
	    	{
	    		e.printStackTrace();
	    	}
	    }catch(Exception e)
	    {
	    	throw new RuntimeException("config.properties does not exits at path: "+ "C:\\Users\\Saura\\eclipse-workspace\\CucumberFramework\\src\\main\\java\\configs\\configuration.properties");
	    }
	}
	
	
	
	public DriverType getDriverType()
	{
		String browserName = properties.getProperty("browserName");
		if(browserName != null || browserName.equalsIgnoreCase("chrome")) return DriverType.CHROME;
		else if(browserName.equalsIgnoreCase("firefox")) return DriverType.FIREFOX;
		else if(browserName.equals("iexplorer")) return DriverType.INTERNETEXPLORER;
		else throw new RuntimeException("Browser Name Key value in Configuration.properties is not matched : " + browserName);
	}
	
	public String getDriverPath()
	{
		String driverPath = properties.getProperty("driverPath");
		if(driverPath!=null)
		{
			return driverPath;
		}
		else 
		{
			throw new RuntimeException("DriverPath is not provided in Properties.configuration file");
		}
	}
	
	public long getImplicitlyWait()
	{
		String implicitlyWait = properties.getProperty("implicitlyWait");
		if(implicitlyWait != null)
		{
			return Integer.parseInt(implicitlyWait);
		}
		else
		{
			throw new RuntimeException("ImplicityWait is not provided in Properties.configuration file");
		}
	}
	
	public String getApplicationUrl()
	{
		String url = properties.getProperty("url");
		if(url != null)
		{
			return url;
		}
	 	else
		{
			throw new RuntimeException("Url is not provided in Properties.configuration file");
		}
	}
}
