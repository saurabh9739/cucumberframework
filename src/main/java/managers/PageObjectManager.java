package main.java.managers;

import org.openqa.selenium.WebDriver;
import main.java.pageObjects.CartPage;
import main.java.pageObjects.CheckoutPage;
import main.java.pageObjects.HomePage;
import main.java.pageObjects.ProductListingPage;

public class PageObjectManager {

	WebDriver driver;
	private HomePage homePage;
	private ProductListingPage productListingPage;
	private CartPage cartPage; 
	private CheckoutPage checkoutPage;
	public PageObjectManager(WebDriver driver)
	{
		this.driver = driver;
	}

	public HomePage getHomePageInstance()
	{
		return (homePage == null) ? homePage = new HomePage(driver) : homePage;
	}
	
	public ProductListingPage getProductListingPageInstance()
	{
		return (productListingPage == null) ? productListingPage = new ProductListingPage(driver) : productListingPage;

	}
	
	public CartPage getCartPageInstance()
	{
		return (cartPage == null) ? cartPage = new CartPage(driver) : cartPage;
	}
	
	public CheckoutPage getCheckoutPageInstance()
	{
		return (checkoutPage == null) ? checkoutPage = new CheckoutPage(driver) : checkoutPage;
	}
}
