package main.java.managers;
import main.java.fileReader.ConfigFileReader;

public class FileReaderManager {
	
	private static FileReaderManager fileReaderManager = new FileReaderManager();
	private FileReaderManager()
	{
	}
	
	public static FileReaderManager getFileReaderManagerInstance()
	{
		return fileReaderManager;
	}
	
	
	private ConfigFileReader configFileReader;
	public ConfigFileReader getConfigFileReaderInstance()
	{
		return(configFileReader == null) ? configFileReader = new ConfigFileReader() :  configFileReader;
	}
}
