 package main.java.managers;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.CapabilityType;

import main.java.enums.DriverType;

public class WebDriverManager
{	
	WebDriver driver;
	DriverType driverType;
	public WebDriver getDriver()
	{
		driverType = FileReaderManager.getFileReaderManagerInstance().getConfigFileReaderInstance().getDriverType();
		if(driverType.toString().equalsIgnoreCase("chrome"))
		{
			System.setProperty("webdriver.chrome.driver", FileReaderManager.getFileReaderManagerInstance().getConfigFileReaderInstance().getDriverPath());
	        ChromeOptions capability = new ChromeOptions();
	        capability.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
	       	capability.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS,true);
	        driver = new ChromeDriver(capability);
	        driver.manage().window().maximize();
	        driver.manage().timeouts().implicitlyWait(FileReaderManager.getFileReaderManagerInstance().getConfigFileReaderInstance().getImplicitlyWait(),TimeUnit.SECONDS);   
		}
		else if(driverType.toString().equalsIgnoreCase("firefox"))
		{
			driver = new FirefoxDriver();
		} 
		else
		{
			throw new RuntimeException("getDriver Method is not able to initiate driver");
		}
		
		return driver;
	}
	
	public void closeDriver()
	{
		driver .close();
	}
}

