package test.java.runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		features = "src\\test\\resources\\functionalTests",
	    glue = {"test\\java\\stepDefinitions"},
		monochrome = true
		
		)
public class TestRunner {

}
