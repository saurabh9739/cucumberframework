package test.java.stepDefinitions;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import main.java.context.TestContext;
import main.java.pageObjects.HomePage;

public class HomePageSteps {
	TestContext testContext;
	HomePage homePage;
	public HomePageSteps(TestContext testContext) {
		this.testContext = testContext;
		homePage = testContext.getPageObjectManager().getHomePageInstance();
	}
	
	@Given("^user is on Home Page$")
	public void user_is_on_Home_Page(){
		homePage.navigateTo_HomePage();
	}
	
	@When("^he search for \"([^\"]*)\"$")
	public void he_search_for(String product)  {
		homePage.perform_Search(product);
	}
}
